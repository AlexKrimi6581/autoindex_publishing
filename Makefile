
default:
    chmod +x entrypoint.sh
    ./entrypoint.sh

build:
	docker build --rm -t flaskdok .

run:
	docker run -v /mnt/documentation:/documentation --net=host --name fldok -i -t flaskdok

clean:
	docker rm fldok
