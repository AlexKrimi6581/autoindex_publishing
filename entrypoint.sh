#! /bin/bash

apt install -y python3-dev
echo "USE TEMPLATER TO SETUP"

>variables.txt
cp vars.txt variables.txt

if [ -z "$INDEX_DIR" ]; then
    echo "use standard INDEX_DIR"
else
    echo "setup new INDEX_DIR :: ${INDEX_DIR}"
    echo "INDEX_DIR=${INDEX_DIR}" >> variables.txt
fi

if [ -z "$INDEX_USER" ]; then
    echo "use standard INDEX_USER"
else
    echo "setup new INDEX_USER :: ${INDEX_USER}"
    echo "INDEX_USER=${INDEX_USER}" >> variables.txt
fi

if [ -z "$INDEX_PASSWORD" ]; then
    echo "use standard INDEX_PASSWORD"
else
    echo "setup new INDEX_PASSWORD"
    echo "INDEX_PASSWORD=${INDEX_PASSWORD}" >> variables.txt
fi

if [ -z "$INDEX_PORT" ]; then
    echo "use standard INDEX_PORT"
else
    echo "setup new INDEX_PORT :: ${INDEX_PORT}"
    echo "INDEX_PORT=${INDEX_PORT}" >> variables.txt
fi

echo -e "$(/bin/bash templater.sh vsftpd.user_list.j2 -f variables.txt)" > vsftpd.user_list
echo -e "$(/bin/bash templater.sh vsftpd.conf.j2 -f variables.txt)" > vsftpd.conf
echo -e "$(/bin/bash templater.sh conf.sh.j2 -f variables.txt)" > conf.sh
echo -e "$(/bin/bash templater.sh run.py.j2 -f variables.txt)" > run.py

chmod +x conf.sh
./conf.sh


# echo -e "$(/bin/bash templater.sh application.j2 -f variables.txt)" > application.yaml