FROM python:3.6.4-slim-jessie

LABEL maintainer="Alexandr Kriminetskiy <kriminetz0810@gmail.com>" \
      description="Documentation publishing service"

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 9999

CMD ["gunicorn", "init", "-b", "0.0.0.0:9999", "--reload", "--timeout",  "100000"]
